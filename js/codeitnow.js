var codePanelsCount = 3;
var htmlHidden = false;
var cssHidden = false;
var jsHidden = false;
var htmlCM = CodeMirror($("#html-part").get(0), {
	value: "<!--html-->",
	mode: "htmlmixed",
	theme: "paraiso-dark",
	lineWrapping: "false",
	lineNumbers: true,
});
var cssCM = CodeMirror($("#css-part").get(0), {
	value: "/*css*/",
	mode: "css",
	theme: "paraiso-dark",
	lineWrapping: "false",
	lineNumbers: true,
});
var jsCM = CodeMirror($("#js-part").get(0), {
	value: "/*JavaScript*/",
	mode: "javascript",
	theme: "paraiso-dark",
	lineWrapping: "false",
	lineNumbers: true,
});

$(document).ready(function () {
	setCodeResultHeight();
});

$(window).resize(function () {
	setCodeResultHeight();
});

var jsConstructor = function (code) {
	var onloadCode = "//<![CDATA[ \n window.onload=function(){ \n" + code + "\n}//]]>";
	return "<script type=\"text/javascript\">" + onloadCode + " \n</script>";
};

var headConstructor = function (cssCode) {
	var meta = "<meta charset=\"UTF-8\">";
	var preffixScript = "<script type=\"text/javascript\" src=\"js/prefixfree.min.js\"></script>";
	var style = "<style>" + cssCode + "</style>";
	return meta + preffixScript + style;
};

$('#runButton').click(function () {
	var htmlCode = htmlCM.getValue();
	var cssCode = cssCM.getValue();
	var jsCode = jsCM.getValue();
	
	var head = headConstructor(cssCode);
	var script = jsConstructor(jsCode);
	
	writeIFrame(head, htmlCode, script);
});

var writeIFrame = function(head, bodyHtml, bodyJS) {
	// find the iframe's document and write some content
	var iframe = document.getElementById("resultFrame");
	var idocument = iframe.contentDocument;
	idocument.open();
	idocument.write("<!DOCTYPE html>");
	idocument.write("<html>");
	idocument.write("<head>" + head + "</head>");
	idocument.write("<body>" + bodyHtml + bodyJS + "</body>");
	idocument.write("</html>");
	idocument.close();
};

var setCodeResultHeight = function() {
	var windowHeight = $(window).height();
	var headerHeight = $('#header').height();
	var editorCodeHeight = $('#editor-code').height();
	var footerHeight = $('#footer').height();
	var codeResultHeight = windowHeight - headerHeight - editorCodeHeight - footerHeight - 5;
	$('#editor-result').height(codeResultHeight + 'px');
};

$('#html-button').click(function () {
	htmlHidden = toggleCodePanels('html',htmlHidden);
});

$('#css-button').click(function () {
	cssHidden = toggleCodePanels('css',cssHidden);
});
$('#js-button').click(function () {
	jsHidden = toggleCodePanels('js',jsHidden);
});

var toggleCodePanels = function (panel,hidden) {
	var el = '#' + panel + '-part';
	var button = '#' + panel + '-button'
	if(hidden) {
		codePanelsCount++;
		$(el).fadeIn();
		$(button).removeClass('non-active');
		return false;
	}
	else {
		if(codePanelsCount > 1) {
			codePanelsCount --;
			$(el).fadeOut();
			$(button).addClass('non-active');
			return true;
		}
	}
	return false;
};